package fsnotify

import (
	"log"

	"github.com/fsnotify/fsnotify"
)

// 熱更新機制
func main() {
	watcher, _ := fsnotify.NewWatcher()
	defer watcher.Close()
	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				log.Panicln("event:", event)
				if event.Op&fsnotify.Write == fsnotify.Write {
					log.Println("modified file:", event.Name)
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()
	pathlisten := "$Home/src/go_gin_project/configs/config.yaml"
	_ = watcher.Add(pathlisten)
	<-done
}
