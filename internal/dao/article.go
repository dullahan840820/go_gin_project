package dao

import (
	"src/go_gin_project/internal/model"
	"src/go_gin_project/pkg/app"
)

func (d *Dao) CountArticle(state uint8) (int, error) {
	article := model.Article{State: state}
	return article.Count(d.engine)
}

func (d *Dao) GetArticleList(state uint8, page, pageSize int) ([]*model.Article, error) {
	article := model.Article{State: state}
	pageOffset := app.GetPageOffset(page, pageSize)
	return article.List(d.engine, pageOffset, pageSize)
}

func (d *Dao) CreateArticle(state uint8, createdBy string, title, desc string) error {
	article := model.Article{
		Title: title,
		Desc:  desc,
		State: state,
		Model: &model.Model{CreatedBy: createdBy},
	}
	return article.Create(d.engine)
}

func (d *Dao) UpdateArticle(id uint32, state uint8, modifiedBy string, title, desc string) error {
	article := model.Article{Model: &model.Model{ID: id}}

	values := map[string]interface{}{
		"state":       state,
		"modified_by": modifiedBy,
		"title":       title,
		"desc":        desc,
	}
	return article.Update(d.engine, values)
}

func (d *Dao) DeleteArticle(id uint32) error {
	article := model.Article{Model: &model.Model{ID: id}}
	return article.Delete(d.engine)
}
