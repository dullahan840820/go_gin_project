package recovery

import (
	"fmt"
	"src/go_gin_project/global"
	"src/go_gin_project/pkg/app"
	"src/go_gin_project/pkg/email"
	"src/go_gin_project/pkg/errcode"
	"time"

	"github.com/gin-gonic/gin"
)

func Recovery() gin.HandlerFunc {
	defailtMailer := email.NewEmail(&email.SMTPInfo{
		Host:     global.EmailSetting.Host,
		Port:     global.EmailSetting.Port,
		IsSSL:    global.EmailSetting.IsSSL,
		UserName: global.EmailSetting.UserName,
		Password: global.EmailSetting.Password,
		From:     global.EmailSetting.From,
	})
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				s := "panic recover err: %v"
				global.Logger.WithCallersFrames().Errorf(s, err)

				err := defailtMailer.SendMail(
					global.EmailSetting.To,
					fmt.Sprintf("異常拋出, 發生時間: %d", time.Now().Unix()),
					fmt.Sprintf("錯誤信息: %v", err),
				)
				if err != nil {
					global.Logger.Panicf("mail.SendMail err: %v", err)
				}
				app.NewResponse(c).ToErrorResponse(errcode.ServerError)
				c.Abort()
			}
		}()
		c.Next()
	}
}
