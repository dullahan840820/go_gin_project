package service

import (
	"src/go_gin_project/internal/model"
	"src/go_gin_project/pkg/app"
)

type CountArticleRequest struct {
	Name  string `form:"name" bind:"max=100"`
	State uint8  `form:"state,default=1" bind:"oneof=0 1"`
}

type ArticleListRequest struct {
	Name  string `form:"name" bind:"max=100"`
	State uint8  `form:"state,default=1" bind:"oneof=0 1"`
}

type CreateArticleRequest struct {
	Title     string `form:"title" binding:"required,min=2,max=100"`
	Desc      string `form:"desc" binding:"required,min=2,max=100"`
	State     uint8  `form:"state,default=1" binding:"oneof=0 1"`
	CreatedBy string `form:"created_by" binding:"required,min=3,max=100"`
}

type UpdateArticleRequest struct {
	ID         uint32 `form:"id" binding:"required,gte=1"`
	Title      string `form:"title" binding:"required,min=2,max=100"`
	Desc       string `form:"desc" binding:"required,min=2,max=100"`
	State      uint8  `form:"state,default=1" binding:"oneof=0 1"`
	ModifiedBy string `form:"modified_by" binding:"required,min=3,max=100"`
}

type DeleteArticleRequest struct {
	ID uint32 `form:"id" binding:"required,gte=1"`
}

func (svc *Service) CountArticle(param *CountArticleRequest) (int, error) {
	return svc.dao.CountArticle(param.State)
}

func (svc *Service) GetArticleList(param *ArticleListRequest, pager *app.Pager) ([]*model.Article, error) {
	return svc.dao.GetArticleList(param.State, pager.Page, pager.PageSize)
}

func (svc *Service) CreateArticle(params *CreateArticleRequest) error {
	return svc.dao.CreateArticle(params.State, params.CreatedBy, params.Title, params.Desc)
}

func (svc *Service) UpdateArticle(params *UpdateArticleRequest) error {
	return svc.dao.UpdateArticle(params.ID, params.State, params.ModifiedBy, params.Title, params.Desc)
}

func (svc *Service) DeleteArticle(params *DeleteArticleRequest) error {
	return svc.dao.DeleteArticle(params.ID)
}
