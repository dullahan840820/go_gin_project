package service

import (
	"src/go_gin_project/global"
	"src/go_gin_project/internal/dao"

	otgorm "github.com/eddycjy/opentracing-gorm"
	"golang.org/x/net/context"
)

type Service struct {
	ctx context.Context
	dao *dao.Dao
}

func New(ctx context.Context) Service {
	svc := Service{ctx: ctx}
	svc.dao = dao.New(otgorm.WithContext(svc.ctx, global.DBEngine))
	return svc
}
