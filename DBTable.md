CREATE TABLE blog_article_tag (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  article_id int(11) NOT NULL COMMENT '文章ID',
  tag_id int(10) unsigned NOT NULL DEFAULT '0' COMMENT '標籤ID',
  created_on int(11) DEFAULT NULL,
  created_by varchar(100) DEFAULT '' COMMENT '创建人',
  modified_on int(10) unsigned DEFAULT '0' COMMENT '修改时间',
  modified_by varchar(255) DEFAULT '' COMMENT '修改人',
  deleted_on int(10) unsigned DEFAULT '0',
  is_del tinyint(3) unsigned DEFAULT '0' COMMENT '是否刪除 0為未刪除. 1為已刪除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章标签關聯';

CREATE TABLE blog_article (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  title varchar(100) DEFAULT '' COMMENT '文章标题',
  desc varchar(255) DEFAULT '' COMMENT '简述',
  cover_image_url varchar(255) DEFAULT '' COMMENT '封面圖片地址',
  content longtext COMMENT '文章內容',
  created_on int(11) DEFAULT NULL,
  created_by varchar(100) DEFAULT '' COMMENT '创建人',
  modified_on int(10) unsigned DEFAULT '0' COMMENT '修改时间',
  modified_by varchar(255) DEFAULT '' COMMENT '修改人',
  deleted_on int(10) unsigned DEFAULT '0',
  is_del tinyint(3) unsigned DEFAULT '0' COMMENT '是否刪除 0為未刪除. 1為已刪除',
  state tinyint(3) unsigned DEFAULT '1' COMMENT '状态 0为禁用1为启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章管理';

CREATE TABLE blog_tag (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(100) DEFAULT '' COMMENT '标签名称',
  created_on int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  created_by varchar(100) DEFAULT '' COMMENT '创建人',
  modified_on int(10) unsigned DEFAULT '0' COMMENT '修改时间',
  modified_by varchar(100) DEFAULT '' COMMENT '修改人',
  deleted_on int(10) unsigned DEFAULT '0',
  is_del tinyint(3) unsigned DEFAULT '0' COMMENT '是否刪除 0為未刪除. 1為已刪除',
  state tinyint(3) unsigned DEFAULT '1' COMMENT '状态 0为禁用、1为启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章标签管理';

CREATE TABLE `blog_auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_key` varchar(20) DEFAULT '' COMMENT 'key',
  `app_secret` varchar(50) DEFAULT '' COMMENT 'Secret',
  `created_on` int(11) DEFAULT '0' COMMENT '建立时间',
  `created_by` varchar(100) DEFAULT '' COMMENT '创建人',
  `modified_on` int(10) unsigned DEFAULT '0' COMMENT '修改时间',
  `modified_by` varchar(255) DEFAULT '' COMMENT '修改人',
  `deleted_on` int(10) unsigned DEFAULT '0',
  `is_del` tinyint(3) unsigned DEFAULT '0' COMMENT '是否刪除 0為未刪除. 1為已刪除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='認證管理';